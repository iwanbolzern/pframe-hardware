﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BinFileCreator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("EDID data has length: {0}", edidData.Length);
            Console.ReadKey();
            File.WriteAllBytes("C://temp//edid.bin", edidData);
        }

        private static byte[] edidData =
            {
                0x00,
                0xFF,
                0xFF,
                0xFF,
                0xFF,
                0xFF,
                0xFF,
                0x00,
                0x0D,
                0xAF,
                0x04,
                0x12,
                0x00,
                0x00,
                0x00,
                0x00,
                0x11,
                0x0F,
                0x01,
                0x03,
                0x80,
                0x19,
                0x12,
                0x78,
                0x0A,
                0xFE,
                0x65,
                0x95,
                0x55,
                0x51,
                0x87,
                0x26,
                0x22,
                0x50,
                0x54,
                0x00,
                0x08,
                0x00,
                0x01,
                0x01,
                0x01,
                0x01,
                0x01,
                0x01,
                0x01,
                0x01,
                0x01,
                0x01,
                0x01,
                0x01,
                0x01,
                0x01,
                0x01,
                0x01,
                0x64,
                0x19,
                0x00,
                0x40,
                0x41,
                0x00,
                0x26,
                0x30,
                0x18,
                0x88,
                0x36,
                0x00,
                0xF6,
                0xB8,
                0x00,
                0x00,
                0x00,
                0x18,
                0x00,
                0x00,
                0x00,
                0xFE,
                0x00,
                0x4E,
                0x31,
                0x32,
                0x31,
                0x58,
                0x35,
                0x2D,
                0x4C,
                0x30,
                0x31,
                0x0A,
                0x20,
                0x20,
                0x00,
                0x00,
                0x00,
                0xFE,
                0x00,
                0x43,
                0x4D,
                0x4F,
                0x0A,
                0x20,
                0x20,
                0x20,
                0x20,
                0x20,
                0x20,
                0x20,
                0x20,
                0x20,
                0x00,
                0x00,
                0x00,
                0xFE,
                0x00,
                0x4E,
                0x31,
                0x32,
                0x31,
                0x58,
                0x35,
                0x2D,
                0x4C,
                0x30,
                0x31,
                0x0A,
                0x20,
                0x20,
                0x00,
                0xA1
            };
    }
}
